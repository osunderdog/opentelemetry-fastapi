import requests

# code from: https://github.com/open-telemetry/opentelemetry-python/blob/main/docs/examples/basic_tracer/basic_trace.py
from opentelemetry import trace
from opentelemetry.propagate import inject
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.cloud_trace import CloudTraceSpanExporter
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
)
import logging
import os
from google.cloud.trace_v2 import BatchWriteSpansRequest, TraceServiceClient
from socket import getfqdn

logger = logging.getLogger(__name__)
logging.basicConfig(level=os.getenv('LOGLEVEL', 'DEBUG'))


tp = TracerProvider()
trace.set_tracer_provider(tp)
# exporter = ConsoleSpanExporter()
gcloud_client = TraceServiceClient()
exporter = CloudTraceSpanExporter(client=gcloud_client)
processor = BatchSpanProcessor(exporter)
trace.get_tracer_provider().add_span_processor(processor)

tracer = trace.get_tracer_provider().get_tracer(__name__)

def main():
    url = "http://localhost:8000/foobar"
    fqdn = getfqdn()
    with requests.session() as sess:
        client_name = f"client-{fqdn}"
        with tracer.start_as_current_span(name=client_name, kind=trace.SpanKind.CLIENT) as client_trace:
            #gotta send the tracer header along with the request.
            # important line.  Needed to set headers in the request that will be picked up on server side trace
            # https://github.com/open-telemetry/opentelemetry-python/blob/main/docs/examples/auto-instrumentation/client.py#L41
            new_headers = {}
            inject(new_headers)
            logger.debug(f"Injected Header Values:{new_headers}")
            # inject(carrier=sess.headers, context=client_trace)
            response = sess.get(url, headers=new_headers)
        response.raise_for_status()
        print(response.json())

    #batch span processor may not flush all before shutting down without asking it to do so.
    tp.force_flush()

if __name__ == '__main__':
    main()
