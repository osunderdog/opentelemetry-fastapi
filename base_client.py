# Copyright The OpenTelemetry Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# from: https://raw.githubusercontent.com/open-telemetry/opentelemetry-python/main/docs/examples/auto-instrumentation/client.py

from sys import argv

import requests
from requests import get

from opentelemetry import trace
from opentelemetry.propagate import inject
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.cloud_trace import CloudTraceSpanExporter
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
)

import logging
import os
from google.cloud.trace_v2 import BatchWriteSpansRequest, TraceServiceClient


logger = logging.getLogger(__name__)
logging.basicConfig(level=os.getenv('LOGLEVEL', 'DEBUG'))

tp = TracerProvider()
trace.set_tracer_provider(tp)
# exporter = ConsoleSpanExporter()
exporter = CloudTraceSpanExporter()
processor = BatchSpanProcessor(exporter)
trace.get_tracer_provider().add_span_processor(processor)

tracer = trace.get_tracer_provider().get_tracer(__name__)

with requests.session() as sess:
    with tracer.start_as_current_span("client"):

        with tracer.start_as_current_span("client-server"):
            new_headers = {}
            inject(new_headers)
            logger.debug(f"Injected Header Values:{new_headers}")
            requested = sess.get(
                "http://localhost:8000/foobar",
                headers=new_headers,
            )

            assert requested.status_code == 200
tp.force_flush()