# Intro

This code represents a minimal fastapi server and a client.
Both the client and server have opentlemetry code that will will trace
the spans created by each fastapi path.

# Authentication

```text
gcloud auth application-default login
gcloud auth application-default revoke
```