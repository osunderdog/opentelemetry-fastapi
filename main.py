
#from https://opentelemetry-python-contrib.readthedocs.io/en/latest/instrumentation/fastapi/fastapi.html
import fastapi
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
from opentelemetry import trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.cloud_trace import CloudTraceSpanExporter
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
)

tp = TracerProvider()
trace.set_tracer_provider(tp)
# exporter = ConsoleSpanExporter()
exporter = CloudTraceSpanExporter()
processor = BatchSpanProcessor(exporter)
trace.get_tracer_provider().add_span_processor(processor)

tracer = trace.get_tracer_provider().get_tracer(__name__)

app = fastapi.FastAPI()

#adds middleware.
FastAPIInstrumentor.instrument_app(app)

@app.get("/foobar")
async def foobar():
    return {"message": "hello world"}

