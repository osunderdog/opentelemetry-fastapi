from typing import Optional
import logging
import os
from pandas import DataFrame
from datetime import datetime
from pathlib import Path
from google.cloud import trace_v1

logger = logging.getLogger(__name__)
logging.basicConfig(level=os.getenv('LOGLEVEL', 'DEBUG'))

DATA_DIRECTORY = Path('./data')


def datetimestamp(time: Optional[datetime] = None) -> str:
    if time is None:
        time = datetime.now()
    return time.strftime('%Y%m%d%H%M%S')

def data_filename(data_path: Path = DATA_DIRECTORY, postfix: str = 'trace', extension: Optional[str] = None) -> Path:
    if extension is None:
        extension = 'data'
    return data_path.joinpath(f"{datetimestamp()}_{postfix}.{extension}")

def main():
    client = trace_v1.TraceServiceClient()

    # 1972-01-01T10:00:20.021-05:00
    query = {
        'project_id': 'gcptraining-221417',
        'start_time': '2022-09-3T00:00:00.000-04:00',
        'end_time': '2022-09-04T12:00:00.000-04:00',
        'view': 'COMPLETE',
        'filter': 'client-'
        # 'page_size': 5
    }

    # strategy: build up a hash per row then hand it to a dataframe
    # Flatten out the project_id and trace_id to index.
    # Try to do all this quickly.
    my_trace_request = trace_v1.ListTracesRequest(**query)
    page_result: trace_v1.pagers.ListTracesPager = client.list_traces(my_trace_request)
    # Handle the response
    response: trace_v1.Trace
    span: trace_v1.types.TraceSpan
    row_list = [{'trace-project_id': response.project_id,
                   'trace-trace_id': response.trace_id,
                   'span-index': span_index,
                   'span-name': span.name,
                   'span-kind': span.kind.name,
                   'span-start_time': span.start_time.rfc3339(),
                   'span-end_time': span.end_time.rfc3339(),
                   } for response in page_result for span_index, span in enumerate(response.spans) ]
    # for row in row_list:
    #     print(row)
    trace_data_df = DataFrame.from_records(row_list)
    print(trace_data_df)
    with data_filename().open("w") as ofh:
        trace_data_df.to_csv(ofh, index=False)



if __name__ == '__main__':
    main()
